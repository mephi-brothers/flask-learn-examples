from flask import Flask, render_template


app = Flask(__name__)
# будем передавать этот список
menu = ["Установка", "Первое приложение", "Связь"]

@app.route("/")
def index():
    # передаем title + menu
    return render_template('index.html', menu = menu)

@app.route("/about")
def about():
    # передаем чисто title
    return render_template('about.html',  title="о сайте", menu=menu)


if __name__ =="__main__":
    app.run(debug=True)
